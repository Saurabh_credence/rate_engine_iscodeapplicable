const fs = require('fs');
const path = require("path");

function isCodeApplicable(custJson, offerJson, transJson){
    try{
        // output object
        let outObj  = { 
            "requestID" : transJson.requestID, 
            "codeType" : offerJson.codeType,
            "validFor" : offerJson.validFor,
            "codeName" : offerJson.codeName,
            "applicable" : "N",
            "message" : ""
        };

        function inArray(arr, val){
            for (x of arr){
                if(x == val){
                    return true
                }
            }
            return false
        }
        
        // Rule 1 
        if(!(inArray(offerJson.termsFilter.channel, (transJson.channel).toLowerCase()))){
            outObj.message = `Channel ${transJson.channel} is Not Applicable for ${offerJson.codeName}, Aviailable Channels [${offerJson.termsFilter.channel}]`;
            return JSON.stringify(outObj);
        }

        // Rule 2
        if(!(inArray(offerJson.termsFilter.transTypeCode, transJson.transTypeCode))){
            outObj.message = `Transaction Type ${transJson.transTypeCode} is Not Applicable for ${offerJson.codeName}, Available Transaction Types [${offerJson.termsFilter.transTypeCode}]`;
            return JSON.stringify(outObj);
        }

        // Rule 3
        let arr1 = offerJson.termsFilter.customerCategory;
        let arr2 = custJson.customerCategory;
        let flag = 0;
        for (let i of arr1){
            for(let j of arr2){
                if (j == i){
                    flag = 1;
                }
            }
        }

        if(!flag){
            outObj.message = `Customer Category [${custJson.customerCategory}] is Not Applicable for ${offerJson.codeName} Available Customer Categories [${offerJson.termsFilter.customerCategory}]`;
            return JSON.stringify(outObj);
        }

        // Rule 4
        let currObj = offerJson.termsFilter.currency;
        let tmpArr = [];
        flag = 0;
        for (let i in currObj){
            if(currObj[i].currCode == transJson.currency){
                flag = 1;
            }
            tmpArr.push(currObj[i].currCode);
        }
        if (!(flag)){
            outObj.message = `Currency ${transJson.currency} is Not Applicable for ${offerJson.codeName} Available Currencies [${tmpArr}]`;
            return JSON.stringify(outObj);
        }

        // Rule 5
        if(offerJson.minMaxAmountType == 'LCY'){
            if(!(parseInt(transJson.lcyAmount) >= offerJson.minimumINRAmount && transJson.lcyAmount <= parseInt(offerJson.maximumINRAmount))){
                outObj.message = `LCY Amount ${transJson.lcyAmount} is Not with in Range ${offerJson.codeName} Range is from ${offerJson.minimumINRAmount} to ${offerJson.maximumINRAmount}`;
                return JSON.stringify(outObj);
            }
        }

        // Rule 6
        if(offerJson.minMaxAmountType == 'FCY'){
            if(!(parseInt(transJson.lcyAmount) >= offerJson.minimumINRAmount && transJson.lcyAmount <= parseInt(offerJson.maximumINRAmount))){
                outObj.message = `FCY Amount ${transJson.lcyAmount} is Not with in Range ${offerJson.codeName} Range is from ${offerJson.minimumINRAmount} to ${offerJson.maximumINRAmount}`;
                return JSON.stringify(outObj);
            }
        }

        // Rule 7
        let startDate = (offerJson.startDateTime).split(' ')[0].split("-");
        let endDate = (offerJson.endDateTime).split(' ')[0].split("-");
        let date = transJson.transDate.split("-");

        let from = new Date(startDate[2], startDate[1], startDate[0]);
        let to = new Date(endDate[2], endDate[1], endDate[0]);
        let check = new Date(date[2], date[1], date[0]);

        if (!((check > from) && (check < to))){
            outObj.message = `Transaction Date ${transJson.transDate} is Not with in Range ${offerJson.codeName} Range is from ${offerJson.startDateTime} to ${offerJson.endDateTime}`;
            return JSON.stringify(outObj);
        }

        // Rule 8
        let usedCodes = custJson.usedCodes;
        flag = 0;
        for (let i in usedCodes){
            if(usedCodes[i].codeType == offerJson.codeType && (parseInt(usedCodes[i].usedCount) < offerJson.maximumUsagePerCustomer)){
                flag = 1;
                break;
            }
        }
        if (!(flag)){
            outObj.message = `Customer has already used maximum no of usages ${offerJson.maximumUsagePerCustomer} available for offer code ${offerJson.codeName}`;
            return JSON.stringify(outObj);
        }

        else{
            outObj.applicable = "Y";
            return JSON.stringify(outObj);
        }
    } 
    catch (err)
    {
        return JSON.stringify({ "status": "unsuccess", "msg": err.message, "applicable": "N" });
    }
}


const custJson = JSON.parse(fs.readFileSync(path.join(__dirname, './json/customerDetails.json'), 'utf-8'));
const offerJson = JSON.parse(fs.readFileSync(path.join(__dirname, './json/selectedOfferCode.json'), 'utf-8'));
const transJson = JSON.parse(fs.readFileSync(path.join(__dirname, './json/transactionDetails.json'), 'utf-8'));

let output = isCodeApplicable(custJson, offerJson, transJson);

//console.log(output); 
module.exports = isCodeApplicable;