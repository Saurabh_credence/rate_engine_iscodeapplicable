const xlxs = require('xlsx');
const isCodeApplicable = require('./index');

function isCodeApplicableTester() {
    try{
        const workbook = xlxs.readFile('Rate Engine Test Cases.xlsx');
        const excel_sheet = workbook.Sheets['IsCodeApplicableForTransaction-'];

        //Array objects
        let expected = [], actual = [];

        //JSON objects
        let custJson, transJson, offerJson, expectedOutJson, actualOutJson;

        //Count total number of rows
        let rowCount = 0;
        for(let x of Object.keys(excel_sheet)){
            if (x[0] == "H"){
                rowCount += 1;
            }
        }

        //Reading data from excel file (row wise)
        let addr;
        for(let i=3; i<=rowCount; i++){
            //getting Customer details JSON (column H)
            addr = "H"+i;
            custJson = (excel_sheet[addr].v).replace('customerDetails=','');
            custJson = (eval('['+custJson+']'))[0];    
            
            //getting transaction details JSON (column I)
            addr = "I"+i;
            transJson = (excel_sheet[addr].v).replace('transactionDetails=','');
            transJson = (eval('['+transJson+']'))[0];

            //getting selected offer code JSON (column J)
            addr = "J"+i;
            offerJson = (excel_sheet[addr].v).replace('selectedOfferCode=','');
            offerJson = (eval('['+offerJson+']'))[0];

            //getting expected Output Json (last column O of row)
            addr = "O"+i;
            expectedOutJson = (excel_sheet[addr].v).replace('IsCodeApplicableForTransaction=','');
            expectedOutJson = (eval('['+expectedOutJson+']'))[0];

            //Calling external Code Applicable function
            actualOutJson = JSON.parse(isCodeApplicable(custJson,offerJson,transJson));

            expected.push(expectedOutJson); 
            actual.push(actualOutJson);
        
        }
        return {"Expected":expected,"Actual":actual};
    }
    catch(err){
        console.log(err);
    }
};


module.exports = isCodeApplicableTester;






