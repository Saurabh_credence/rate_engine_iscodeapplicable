const fs = require('fs');
const path = require('path');
const isCodeApplicable = require('../index');
const { assert } = require('chai');

const custJson = JSON.parse(fs.readFileSync(path.join(__dirname, '../json/customerDetails.json'), 'utf-8'));
const offerJson = JSON.parse(fs.readFileSync(path.join(__dirname, '../json/selectedOfferCode.json'), 'utf-8'));
const transJson = JSON.parse(fs.readFileSync(path.join(__dirname, '../json/transactionDetails.json'), 'utf-8'));

const expectedOutJson = { 
    "requestID":"1",
    "codeType":"D",
    "validFor":"RC",
    "codeName":"STUDENT",
    "applicable" : "Y",
    "message" : ""
};

// describe('Discount code validation testing', function () {
//     const actualOutJson = JSON.parse(isCodeApplicable(custJson, offerJson, transJson));

//     it('Actual output should match with expected output', function () {
//         assert.deepEqual(actualOutJson, expectedOutJson);
//     });
// });