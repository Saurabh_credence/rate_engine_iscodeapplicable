const { assert } = require('chai');
const isCodeApplicableTester = require('../tester');
let data = isCodeApplicableTester();

describe('Testing the Is Code Applicable function',function(){  
    for(let i=0;i<data.Expected.length;i++){
        describe(`Testing : Test case no.${i + 1}`,function(){
            it('Actual JSON output should match with Expected JSON output',function(){
                assert.deepEqual(data.Actual[i],data.Expected[i]);
            })
        });
    }
});